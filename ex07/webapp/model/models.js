sap.ui.define([
	"sap/ui/model/json/JSONModel",
	"sap/ui/Device"
], function(JSONModel, Device) {
	"use strict";

	return {

		createDeviceModel: function() {
			var oModel = new JSONModel(Device);
			oModel.setDefaultBindingMode("OneWay");
			return oModel;
		},
		createJSONModel: function() {
			var json = {
				firstName: "Louise",
				lastName: "Huang",
				nameList: [
					{firstName: "Steve", lastName: "Jobs"},
					{firstName: "Daniel", lastName: "Jackson"},
					{firstName: "Sam", lastName: "Carter"}
				]
			};
			var oModel = new JSONModel(json);
			return oModel;
		}

	};
});