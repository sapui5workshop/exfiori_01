sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("com.demo.s276.ex01EXFIORI_01.controller.Main", {
		onBtnPress:function() {
			// var firstName = this.byId("inpFirstName").getValue(); // 取得 First Name 的值
			// var lastName = this.byId("inpLastName").getValue();   // 取得 Last Name 的值
			var firstName = this.getView().getModel().getProperty("/firstName"); // 從 Model 取得 First Name 的值
			var lastName = this.getView().getModel().getProperty("/lastName");   // 從 Model 取得 Last Name 的值
			this.byId("txtDisplay").setText("Hello, " + firstName + " " + lastName + "!"); // 組合字串並呈現到 txtDisplay 中
		}
	});
});